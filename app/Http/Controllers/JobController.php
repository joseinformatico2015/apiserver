<?php

namespace App\Http\Controllers;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Job;
class JobController extends Controller
{
    public function index(Request $request)
    {
        if(isset($request->command)){
            switch ($request->command) {
                case 'create':
                        return $this->store($request);
                    break;
                case 'check':
                        return $this->check($request);
                    break;
                case 'pick':
                        return $this->pick($request);
                    break;
                case 'complete':
                        return $this->complete($request);
                    break;
                case 'average':
                    return $this->average($request);
                break;
                default:
                    return response()
                    ->json(['errors'=>array(['code'=>404,'message'=>'you need to pass a valid COMMAND, my friend. check the manual!!!'])],404);
                    break;
            }
        }else{
            return response()
            ->json(['errors'=>array(['code'=>404,'message'=>'you need to pass a COMMAND in your request my friend. check the manual!!!'])],404);
        }

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'submitter_id' => 'required',
            // 'processor_id' => 'required',
            // 'command' => 'required',
            // 'status' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()
            ->json(['errors'=>array(['code'=>404,'message'=>'need to pass submitter_id and description to create a new Job.'])],404);
        }

        $job = Job::create($request->all());
        $job->status='non-completed';
        $job->save();
        // return  $job;
        $JsonJob['id']=$job->id;
        $JsonJob['submitter_id']=(int)$job->submitter_id;
        $JsonJob['description']=$job->description;
        $JsonJob['status']=$job->status;
        return response()->json(['message'=>'you have created a new Job','Job :'=>$JsonJob],200);
    }

    public function check(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'job_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()
            ->json(['errors'=>array(['code'=>404,'message'=>'need to pass job_id to check a Job'])],404);
        }
        if (Job::where('id', $request->job_id)->exists()) {
            $job = Job::where('id', $request->job_id)->first();
            $JsonJob['id']=$job->id;
            $JsonJob['submitter_id']=(int)$job->submitter_id;
            $JsonJob['processor_id']=$job->processor_id;
            $JsonJob['description']=$job->description;
            $JsonJob['status']=$job->status;
            return response()->json(['message'=>'you are checking status Jobs','Job :'=>$JsonJob],200);
        } else {
            return response()->json(["message" => "Job not found"], 404);
        }
    
    }
    public function pick(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'processor_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()
            ->json(['errors'=>array(['code'=>404,'message'=>'need to pass processor_id, for a Job assignment'])],404);
        }
        if (Job::where('processor_id',$request->processor_id)->exists()) {
            return response()->json(['code'=>404,'message'=>'Oh!!! you already has a Job assigned'],404);
        }else{
            if (Job::where('status','non-completed')->exists()) {
                $job = Job::where('status','non-completed')->first();
                $job->processor_id=$request->processor_id;
                $job->status='in-process';
                $job->save();
                $JsonJob['id']=$job->id;
                $JsonJob['submitter_id']=(int)$job->submitter_id;
                $JsonJob['processor_id']=(int)$job->processor_id;
                $JsonJob['description']=$job->description;
                $JsonJob['status']=$job->status;
                return response()->json(['message'=>'Now this Job is process By You','Job :'=>$JsonJob],200);
            } else {
                return response()->json([
                    "message" => "no Jobs available"
                ], 404);
            }
        }
    }
    public function complete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'processor_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()
                ->json(['errors'=>array(['code'=>404,'message'=>'need to pass processor_id, if you want complete a Job'])],404);
            }
            
        if (Job::where('processor_id',$request->processor_id)->exists()) {
            
            $job = Job::where('processor_id',$request->processor_id)->first();
            $job->status='complete';
            $job->save();
            $JsonJob['id']=$job->id;
            $JsonJob['submitter_id']=(int)$job->submitter_id;
            $JsonJob['processor_id']=(int)$job->processor_id;
            $JsonJob['description']=$job->description;
            $JsonJob['status']=$job->status;
            return response()->json(['message'=>'Greate!!! this Job is complete, go and pick another one','Job :'=>$JsonJob],200);
        }else{
            return response()->json(['code'=>404,'message'=>'You dont have any Job assigned right now'],404);
        }
    }

    public function average(Request $request)
    {
            $Tdays=0;
            $Thours=0; 
            $Tminutes=0;
            $Tseconds=0;

            $jobs = Job::where('status','complete')->get();
            foreach ($jobs as $job) {
                // $created_at, $updated_at
                $days = $job->created_at->diffInDays($job->updated_at);
                $hours = $job->created_at->diffInHours($job->updated_at->subDays($days));
                $minutes = $job->created_at->diffInMinutes($job->updated_at->subHours($hours));
                $seconds = $job->created_at->diffInSeconds($job->updated_at->subMinutes($minutes));

                $Tdays=$Tdays+$days;
                $Thours=$Thours+$hours;
                $Tminutes=$Tminutes+$minutes;
                $Tseconds=$Tseconds+$seconds;
            }
            $totalJobs=$jobs->count();
            $Tdays=$Tdays/$totalJobs;
            $Thours=$Thours/$totalJobs;
            $Tminutes=$Tminutes/$totalJobs;
            $Tseconds=$Tseconds/$totalJobs;

            return response()->json(['message'=>'The average processing time is:','result'=>CarbonInterval::days($Tdays)->hours($Thours)->minutes($Tminutes)->forHumans()],200);
            // return CarbonInterval::days($Tdays)->hours($Thours)->minutes($Tminutes)->seconds($Tseconds)->forHumans();
    }
}
