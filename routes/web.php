<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::post('/jobs', 'JobController@store')->name('job.store');
Route::get('/jobs/{id}/{command}', 'JobController@check')->name('job.check');
Route::get('/jobs/pick/{id}/{command}', 'JobController@pick')->name('job.pick');
Route::get('/jobs', 'JobController@crear')->name('job.crear');
